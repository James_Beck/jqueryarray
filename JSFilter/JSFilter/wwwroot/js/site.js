﻿// Write your JavaScript code.
$('#HideBlue').click(function () {    
    if ($(this).text() == 'Hide Blue')
    {
        $('div').filter(".blue").css("display", "none");
        $('#HideBlue').text("Show Blue");
    }
    else
    {
        $('div').filter(".blue").css("display", "block");
        $('#HideBlue').text("Hide Blue");
    }
});

$('#HideRed').click(function () {
    if ($(this).text() == 'Hide Red') {
        $('div').filter(".red").css("display", "none");
        $(this).text("Show Red");
    }
    else {
        $('div').filter(".red").css("display", "block");
        $(this).text("Hide Red");
    }
});

$('#HideGreen').click(function () {
    if ($(this).text() == 'Hide Green') {
        $('div').filter(".green").css("display", "none");
        $(this).text("Show Green");
    }
    else {
        $('div').filter(".green").css("display", "block");
        $(this).text("Hide Green");
    }
});

$('#HideYellow').click(function () {
    if ($(this).text() == 'Hide Yellow') {
        $('div').filter(".yellow").css("display", "none");
        $(this).text("Show Yellow");
    }
    else {
        $('div').filter(".yellow").css("display", "block");
        $(this).text("Hide Yellow");
    }
});

//
